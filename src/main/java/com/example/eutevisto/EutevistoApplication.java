package com.example.eutevisto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EutevistoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EutevistoApplication.class, args);
	}
}
