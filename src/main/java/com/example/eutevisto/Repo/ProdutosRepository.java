package com.example.eutevisto.Repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.eutevisto.Model.Produtos;

public interface ProdutosRepository extends JpaRepository<Produtos, Long> {

}
