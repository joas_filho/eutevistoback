package com.example.eutevisto.Repo;

import java.util.List;

import javax.swing.Spring;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.eutevisto.Model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Spring> {
	
	List<Usuario>  findByEmail(String email);

}
