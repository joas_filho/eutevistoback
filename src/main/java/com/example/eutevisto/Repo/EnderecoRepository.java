package com.example.eutevisto.Repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.eutevisto.Model.Endereco;

public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

}
