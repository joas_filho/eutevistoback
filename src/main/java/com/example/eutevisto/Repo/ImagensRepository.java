package com.example.eutevisto.Repo;


import org.springframework.data.jpa.repository.JpaRepository;

import com.example.eutevisto.Model.Imagens;

public interface ImagensRepository extends JpaRepository<Imagens, Long> {

}
