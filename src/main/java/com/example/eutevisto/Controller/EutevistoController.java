package com.example.eutevisto.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.eutevisto.Model.Endereco;
import com.example.eutevisto.Model.Imagens;
import com.example.eutevisto.Model.Produtos;
import com.example.eutevisto.Model.Usuario;
import com.example.eutevisto.Repo.EnderecoRepository;
import com.example.eutevisto.Repo.ImagensRepository;
import com.example.eutevisto.Repo.ProdutosRepository;
import com.example.eutevisto.Repo.UsuarioRepository;


@RestController
@RequestMapping("/api")
public class EutevistoController {
	@Autowired
	private EnderecoRepository endereco;
	
	@Autowired
	private UsuarioRepository Usuario;
	
	@Autowired
	private ProdutosRepository Produtos;
	
	@Autowired
	private ImagensRepository Imagens;
	
		
	@GetMapping("/endereco")
	public Iterable<Endereco> getAllEndereco() {
		 Iterable<Endereco> ListaDeEnderecos = endereco.findAll();
	     return ListaDeEnderecos;
	}
	
	@GetMapping("/usuario")
	public Iterable<Usuario> getAllUsuarioFisico() {
		 Iterable<Usuario> ListaDeUsuarios = Usuario.findAll();
	     return ListaDeUsuarios;
	}
	
	@GetMapping(value = "usuario/lista/{email}")
	public List<Usuario> findByEmail(@PathVariable String email) {

		List<Usuario> usuarios = Usuario.findByEmail(email);
		
		return usuarios;
	}
	
	@GetMapping("/imagens")
	public Iterable<Imagens> getAllimagens() {
		 Iterable<Imagens> ListaDeImagens = Imagens.findAll();
	     return ListaDeImagens;
	}
	
	@GetMapping("/produtos")
	public Iterable<Produtos> getAllchamado() {
		 Iterable<Produtos> ListaDeProdutos = Produtos.findAll();
	     return ListaDeProdutos;
	}
	
	
	
	@PostMapping(value = "/endereco/save")
	public Endereco postEndereco(@RequestBody Endereco enderecos) {

		Endereco _customer = endereco.save(new Endereco(enderecos.getId() , enderecos.getRua(),enderecos.getBairro(),enderecos.getEstado(),enderecos.getCidade(),enderecos.getNumero(),enderecos.getCep(),enderecos.getComplemento(),enderecos.getId_usuario()) );
		return _customer;
	}
	
	@PostMapping(value = "/usuario/save")
	public Usuario postUsuario(@RequestBody Usuario usuario) {

		Usuario _customer = Usuario.save(new Usuario(usuario.getId() , usuario.getNome(),usuario.getSobrenome(),usuario.getCpf(),usuario.getTelefone(),usuario.getDDD(),usuario.getSexo(),usuario.getEmail(),usuario.getSenha()));
		return _customer;
	}
	
	
	@PostMapping(value = "/produtos/save")
	public Produtos postProdutos(@RequestBody Produtos produtos) {

		Produtos _customer = Produtos.save(new Produtos(produtos.getId() , produtos.getCor(),produtos.getSobrenome(),usuario.getCpf(),usuario.getTelefone(),usuario.getDDD(),usuario.getSexo(),usuario.getEmail(),produtos.getId_usuario(),produtos.getId_categoria(),produtos.getTipo()));
		return _customer;
	}
	
	@PostMapping(value = "/metas/save")
	public Metas postmetas(@RequestBody Metas meta) {

		Metas _customer = metas.save(new Metas(meta.getId(),meta.getPeso(),meta.getMetrica_bracoE(),meta.getMetrica_bracoD(),meta.getMetrica_coxaE(),meta.getMetrica_coxaD(),meta.getMetrica_pantorrilhae(),meta.getMetrica_pantorrilhad(),meta.getMetrica_cintura(),meta.getMetrica_peitoral(),meta.getMetrica_gluteos(),meta.getMetrica_antebracoE(),meta.getMetrica_antebracoD(),meta.getMetrica_ombros(),meta.getIndice_gordura(),meta.getFlexibilidade(),meta.getCd_usuario(),meta.getData_inicial(),meta.getData_final()));
		return _customer;
	}
	
		
}

