package com.example.eutevisto.Model;

import java.util.InputMismatchException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table( name = "usuario")
public class Usuario {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "nome_usuario")
    private String nome;
    @Column(name = "sobrenome_usuario")
    private String sobrenome;
    @Column(name = "CPF")
    private String cpf;
    @Column(name = "telefone_usuario")
    private String telefone;
    @Column(name = "DDD")
    private String DDD;
    @Column(name = "sexo_usuario")
    private String sexo;
    @Column(name = "email")
    private String email;
    @Column(name = "senha")
    private String senha;
    
	
    public Usuario(int id, @NotBlank String nome, @NotBlank String sobrenome, @NotBlank String cpf,
			String telefone, String DDD, @NotBlank String sexo, @NotBlank String email, @NotBlank String senha) {
		super();
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.cpf = cpf;
		this.telefone = telefone;
		this.DDD = DDD;
		this.sexo = sexo;
		this.email = email;
		this.senha = senha;
		
		
    }
		public Usuario() {
	        super();
	        // TODO Auto-generated constructor stub
	    }
		public int getId() {
			return id;
		}
		public void setId_usuario(int id) {
			this.id = id;
		}
		public String getNome() {
			return nome;
		}
		public void setNome(String nome) {
			this.nome = nome;
		}
		public String getSobrenome() {
			return sobrenome;
		}
		public void setSobrenome(String sobrenome) {
			this.sobrenome = sobrenome;
		}
		public String getCpf() {
			return cpf;
		}
		public void setCpf(String cpf) {
			this.cpf = cpf;
		}
		public String getTelefone() {
			return telefone;
		}
		public void setTelefone(String telefone) {
			this.telefone = telefone;
		}
		public String getDDD() {
			return DDD;
		}
		public void setDDD(String dDD) {
			DDD = dDD;
		}
		public String getSexo() {
			return sexo;
		}
		public void setSexo(String sexo) {
			this.sexo = sexo;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getSenha() {
			return senha;
		}
		public void setSenha(String senha) {
			this.senha = senha;
		}
		
		public static boolean isCPF(String CPF) {
	        // considera-se erro CPF's formados por uma sequencia de numeros iguais
	        if (CPF.equals("00000000000") ||
	                CPF.equals("11111111111") ||
	                CPF.equals("22222222222") || CPF.equals("33333333333") ||
	                CPF.equals("44444444444") || CPF.equals("55555555555") ||
	                CPF.equals("66666666666") || CPF.equals("77777777777") ||
	                CPF.equals("88888888888") || CPF.equals("99999999999") ||
	                (CPF.length() != 11))
	            return(false);

	        char dig10, dig11;
	        int sm, i, r, num, peso;

	        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
	        try {
	            // Calculo do 1o. Digito Verificador
	            sm = 0;
	            peso = 10;
	            for (i=0; i<9; i++) {
	                // converte o i-esimo caractere do CPF em um numero:
	                // por exemplo, transforma o caractere '0' no inteiro 0
	                // (48 eh a posicao de '0' na tabela ASCII)
	                num = (int)(CPF.charAt(i) - 48);
	                sm = sm + (num * peso);
	                peso = peso - 1;
	            }

	            r = 11 - (sm % 11);
	            if ((r == 10) || (r == 11))
	                dig10 = '0';
	            else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

	            // Calculo do 2o. Digito Verificador
	            sm = 0;
	            peso = 11;
	            for(i=0; i<10; i++) {
	                num = (int)(CPF.charAt(i) - 48);
	                sm = sm + (num * peso);
	                peso = peso - 1;
	            }

	            r = 11 - (sm % 11);
	            if ((r == 10) || (r == 11))
	                dig11 = '0';
	            else dig11 = (char)(r + 48);

	            // Verifica se os digitos calculados conferem com os digitos informados.
	            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
	                return(true);
	            else return(false);
	        } catch (InputMismatchException erro) {
	            return(false);
	        }
	    }

	    public static String imprimeCPF(String CPF) {
	        return(CPF.substring(0, 3) + "." + CPF.substring(3, 6) + "." +
	                CPF.substring(6, 9) + "-" + CPF.substring(9, 11));
	    }
	

}

