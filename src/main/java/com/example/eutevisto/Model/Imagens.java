package com.example.eutevisto.Model;

import java.io.FileInputStream;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table (name = "imagens")
public class Imagens {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	@NotBlank
	@Column(name = "id_produto")
	private int id_produto;
	@NotBlank
	@Column(name = "imagem_produto")
	private FileInputStream imagem_produto;
	
	public Imagens (@NotBlank int id, @NotBlank int id_produto, @NotBlank FileInputStream imagem_produto) {
		super();
		
		this.id = id;
		this.id_produto = id_produto;
		this.imagem_produto = imagem_produto;
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_produto() {
		return id_produto;
	}

	public void setId_produto(int id_produto) {
		this.id_produto = id_produto;
	}

	public FileInputStream getImagem_produto() {
		return imagem_produto;
	}

	public void setImagem_produto(FileInputStream imagem_produto) {
		this.imagem_produto = imagem_produto;
	}
	
	
}

