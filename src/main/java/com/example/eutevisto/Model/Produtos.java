package com.example.eutevisto.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table( name = "produtos")
public class Produtos {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private int id;
	    @NotBlank
	    @Column(name = "cor_produto")
	    private String cor;
	    @NotBlank
	    @Column(name = "tamanho_produto")
	    private String tamanho;
	    @NotBlank
	    @Column(name = "nome_produto")
	    private String nomeproduto;
	    @NotBlank
	    @Column(name = "descricao_produto")
	    private String descricao;
	    @NotBlank
	    @Column(name = "valor_produto")
	    private Double valor;
	    @NotBlank
	    @Column(name = "id_usuario")
	    private int id_usuario ;
	    @NotBlank
	    @Column(name = "id_tipo")
	    private int tipo;
	    @NotBlank
	    @Column(name = "id_categotia")
	    private int id_categoria;
	    
	    public Produtos(int id, @NotBlank String cor, @NotBlank String tamanho, @NotBlank String nomeproduto,
				@NotBlank String descricao, @NotBlank  Double valor, @NotBlank int tipo, @NotBlank int id_categoria,
			 int id_usuario) {
	    	
			super();
			this.id = id;
			this.cor = cor;
			this.tamanho = tamanho;
			this.nomeproduto = nomeproduto;
			this.descricao = descricao;
			this.valor = valor;
			this.tipo = tipo;
			this.id_categoria = id_categoria;
			this.id_usuario = id_usuario;
			this.id_usuario = id_usuario;
		}

		public Produtos() {
	        super();
	    }

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getCor() {
			return cor;
		}

		public void setCor(String cor) {
			this.cor = cor;
		}

		public String getTamanho() {
			return tamanho;
		}

		public void setTamanho(String tamanho) {
			this.tamanho = tamanho;
		}

		public String getNomeproduto() {
			return nomeproduto;
		}

		public void setNomeproduto(String nomeproduto) {
			this.nomeproduto = nomeproduto;
		}

		public String getDescricao() {
			return descricao;
		}

		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}

		public Double getValor() {
			return valor;
		}

		public void setValor(Double valor) {
			this.valor = valor;
		}

		public int getId_usuario() {
			return id_usuario;
		}

		public void setId_usuario(int id_usuario) {
			this.id_usuario = id_usuario;
		}

		public int getTipo() {
			return tipo;
		}

		public void setTipo(int tipo) {
			this.tipo = tipo;
		}

		public int getId_categoria() {
			return id_categoria;
		}

		public void setId_categoria(int id_categoria) {
			this.id_categoria = id_categoria;
		}
	    


	
}
