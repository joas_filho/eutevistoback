package com.example.eutevisto.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table(name = "endereco")
public class Endereco {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @NotBlank
    @Column(name = "rua")
    private String rua;
    @NotBlank
    @Column(name = "bairro")
    private String bairro;
    @NotBlank
    @Column(name = "estado")
    private String estado;
    @NotBlank
    @Column(name = "cidade")
    private String cidade;
    @NotBlank
    @Column(name = "numero")
    private int numero;
    @NotBlank
    @Column(name = "cep")
    private int cep;
    @NotBlank
    @Column(name = "complemento")
    private String complemento;
    @NotBlank
    @Column(name = "id_usuario")
    private int id_usuario;

   
    public Endereco(int id, @NotBlank String rua, @NotBlank String bairro, @NotBlank String estado,
			@NotBlank String cidade,  int numero,  int cep, String complemento,
		 int id_usuario) {
		super();
		this.id = id;
		this.rua = rua;
		this.bairro = bairro;
		this.estado = estado;
		this.cidade = cidade;
		this.numero = numero;
		this.cep = cep;
		this.complemento = complemento;
		this.id_usuario = id_usuario;
	}

	public Endereco() {
        super();
    }

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public int getCep() {
		return cep;
	}

	public void setCep(int cep) {
		this.cep = cep;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	
    
}